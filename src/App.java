
public class App {
    public static void main(String[] args) throws Exception {

        Employee employee1 = new Employee(1, "Tam", "Nguyen", 100000);
        Employee employee2 = new Employee(1, "Minh", "Khai", 500000);
        employee1.raiseSalary(20 / 100);
        employee2.raiseSalary(30 / 100);
        System.out.println("Luong hằng năm:" + employee1.getAnualSalary());
        System.out.println("Luong hằng năm:" + employee2.getAnualSalary());
        System.out.println(
                "Luong hằng năm:" + employee1.toString() + " được tăng lên " + employee1.raiseSalary(20));
        System.out.println(
                "Luong hằng năm:" + employee2.toString() + " được tăng lên " + employee2.raiseSalary(10));
    }

}